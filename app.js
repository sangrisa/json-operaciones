const argv = require('./config/yargs.js').argv;
const porhacer = require('./tareas/tarea.js');
let consultar = require('./consultar.js');
let tarea;
const dato = argv._[0];
const colors = require('colors');

switch (dato) {
    case 'crear':
        console.log('Creando tarea...');
        tarea = porhacer.creartarea(argv.d);
        console.log();
        console.log('Visualizacion de la tarea :');
        console.log();
        console.log(tarea);
        break;
    case 'listar':
        console.log('Listando tareas...');
        consultar.listar(tarea);
        break;
    case 'actualizar':
        console.log('Actualizando tarea...');
        let tarea3 = porhacer.actualizartarea(argv.d, argv.c);
        console.log();
        console.log('Visualizacion de la tarea :');
        console.log();
        console.log('Completado : '.yellow + tarea3);
        break;
    case 'borrar':
        console.log('Eliminando tarea...');
        let tarea4 = porhacer.borrar(argv.d);
        console.log();
        console.log('Tarea Borrada : '.red + tarea4);
        console.log();
        break;
    case 'filtrar':
        console.log('Listando por completado...');
        let tarea7 = porhacer.filtrar(argv.c);
        console.log(tarea7);
        break;
    default:
        console.log('Comando invalido, vuelvalo a intentar');
        break;
}