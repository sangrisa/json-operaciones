const fs = require('fs');
const colors = require('colors');

let listadoporhacer = [];

const guardar = () => {

    let data = JSON.stringify(listadoporhacer);
    fs.writeFile('./db/file.json', data, (err) => {
        if (err) {
            console.log(err.red);
        }
    })
};

const creartarea = (descripcion) => {

    cargardb();

    let porhacer = {
        descripcion,
        completado: false,
    };

    listadoporhacer.push(porhacer);

    guardar();


    return porhacer;
};

const cargardb = () => {
    try {
        listadoporhacer = require('../db/file.json');
    } catch (err) {
        listadoporhacer = [];
    }
};


let listartarea = () => {
    try {
        listadoporhacer = require('../db/file.json');
    } catch (err) {
        listadoporhacer = [];
    }
    return listadoporhacer;
};

let actualizartarea = (descripcion, completado) => {

    cargardb();

    let index = listadoporhacer.findIndex(tarea => tarea.descripcion === descripcion);

    if (index < 0) {
        console.log('No se ha encontrado la descripcion buscada');
        return false;
    } else {
        listadoporhacer[index].completado = completado;
        guardar();
        return true;
    }
};

let borrar = (descripcion) => {

    cargardb();

    let nuevolistado = listadoporhacer.filter(tarea => {
        return tarea.descripcion !== descripcion;
    });

    if (listadoporhacer === nuevolistado) {
        return false;
    } else {
        listadoporhacer = nuevolistado;
        guardar();
        return true;
    }
};

let filtrar = (completado) => {
    cargardb();
    let cont;
    //completado = (completado == "true");
    let buscar = listadoporhacer.filter(tarea => String(tarea.completado) === String(completado));
    cont = buscar.length;
    console.log('\nCantidad de entradas concretadas : '.yellow + cont);
    return buscar;
};

module.exports = {
    creartarea,
    listartarea,
    actualizartarea,
    borrar,
    filtrar,
}