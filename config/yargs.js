const colors = require('colors');

const opts = {
    descripcion: {
        alias: 'd',
        demand: true,
        default: '',
        desc: 'Descripcion de la tarea',
    },
    completado: {
        alias: 'c',
        demand: true,
        default: true,
        desc: 'La tarea se finaliza o no',
    },
};

const argv = require('yargs')
    .command('Crear', 'Se crea la tarea'.magenta, opts)
    .command('Listar', 'Se listan las tareas almacenadas'.magenta, opts)
    .command('Actualizar', 'Se actualizan los campos en la base de datos'.magenta, opts)
    .command('Borrar', 'Se borran las tareas de la base de datos'.magenta, opts)
    .command('Filtrar por completado', 'Se filtran los resultados por completado'.magenta, opts)
    .help()
    .argv;

module.exports = {
    argv
}